<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$enabled = true;
define('MINIMUM_PHP', '5.3.10');

try
{
	if (version_compare(PHP_VERSION, MINIMUM_PHP, '<'))
	{
		throw new Exception('001: Your host needs to use PHP ' . MINIMUM_PHP . ' or higher');
	}
	else if (version_compare(PHP_VERSION, MINIMUM_PHP, '<') || $enabled == false)
	{
		throw new Exception('002: Geen toegang');
	}
	else
	{
		if (empty($_SERVER['HTTPS']))
		{
			header('Location: https://' . str_replace('www.', '', $_SERVER['SERVER_NAME']));
			
			die();
		}
		else if (!empty($_SERVER['HTTPS']))
		{
			include_once('functions.php');
			
			$app = new RayonSite();
			$app->getIndex();
		}
	}
}

catch (Exception $error)
{
	echo "<html>
	<head>
	<title>Error</title>
	</head>
	<body>
	Error: " . $error->getMessage()
	. "</body>
	</html>";
	
	ini_set("log_errors", 1);
	ini_set("error_log", "php-error.log");
	error_log(date('l jS \of F Y h:i:s A') . " - " . $error->getMessage() . " - " . $_SERVER['REMOTE_ADDR']);
	
	return false;
}
?>