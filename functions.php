<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class RayonSite
{
	function getIndex()
	{		
		echo '<html>
		<!DOCTYPE html>
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl-nl" lang="nl-nl">
     	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	   	<meta name="keywords" content="Rayon 16, rayon 16, Rayon16, rayon16" />
		<meta name="generator" content="Rayon 16" />
		<base href="https://www.rayon16.nl/" />
		<title>' . 'Rayon 16' . '</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		</head>
		<body>
		<iframe src="main.php" style="position:fixed; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
		</body>
		</html>';
	}
	
	function getInfo($value)
	{
		if ($value == 'name')
		{
			$name = "Rayon 16";
			
			return $name;
		}
		
		if ($value == 'owner')
		{
			$owner = "William D. Edwards";
			
			return $owner;
		}
		
		if ($value == 'linkedin')
		{
			$linkedin = 'https://www.linkedin.com/in/williamdavidedwards';
			
			return $linkedin;
		}
		
		if ($value == 'address')
		{
			$street = 'Theseusweg 6';
			$zip = '5631 KS';
			$city = 'Eindhoven';
			$city = strtoupper($city);
			
			$address = $street . ', ' . $zip . '&nbsp; ' . $city;
			
			return $address;
		}
		
		if ($value == 'kvk')
		{
			$kvknr = '61334146';
			$extra = 'onderdeel van Cyberfusion';
			
			$kvk = $kvknr . ' (' . $extra . ')';
			
			return $kvk;
		}
		
		if ($value == 'btw')
		{
			$btwnr = '242955411B01';
			$btwcountry = 'NL';
			
			$btw = $btwcountry . ' ' . $btwnr;
			
			return $btw;
		}
		
		if ($value == 'iban')
		{
			$iban = 'NL23RABO0301717850';
			
			return $iban;
		}
		
		if ($value == 'phone')
		{
			$phone = '+31 (6) 18 68 12 25';
			
			return $phone;
		}
		
		if ($value == 'git')
		{
			$git = 'https://bitbucket.org/williamdavidedwards/rayon-16-site/src/';
			
			return $git;
		}
	}
	
	function getHandelsnamen()
	{
		$handelsnaam['1'] = "Cyberfusion";
		$handelsnaam['2'] = "BedrijfsWerkplek";
		$handelsnaam['3'] = "SG Mine/Arcade";
		$handelsnaam['4'] = "openIcarus";
		$handelsnaam['5'] = "";
		$handelsnaam['6'] = "";
		
		return $handelsnaam;
	}
}