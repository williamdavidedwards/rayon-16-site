<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once('functions.php');

$app = new RayonSite();

$handelsnamen = $app->getHandelsnamen();
?>
<html>
		<!DOCTYPE html>
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl-nl" lang="nl-nl">
     	<head>
		<link rel="stylesheet" type="text/css" href="css/site.css">
     	</head>

	<body>
		<div class="container">
		<div class="menu menu-horizontal">
			    <ul class="menu-list">
			        <li class="menu-item"><a href="/" class="menu-link">Bedrijfsgegevens</a></li>
					<li class="menu-item"><a href="https://cyberfusion.nl" class="menu-link" target="_blank"><?print_r($handelsnamen['1'])?></a></li>
					<li class="menu-item"><a href="https://bedrijfswerkplek.nl" class="menu-link" target="_blank"><?print_r($handelsnamen['2'])?></a></li>
					<li class="menu-item"><a href="https://minearcade.nl" class="menu-link" target="_blank"><?print_r($handelsnamen['3'])?></a></li>
			    </ul>
			</div>

			<div class="content">
				<h2><?print_r($app->getInfo('name'));?></h2>
				<p>
					Naam: <?print_r($app->getInfo('name'));?><br />
					Eigenaar: <a href="<?print_r($app->getInfo('linkedin'));?>" target="_blank"><?print_r($app->getInfo('owner'));?></a><br />
					Adres: <?print_r($app->getInfo('address'));?><br />
					KvK: <?print_r($app->getInfo('kvk'));?><br />
					Btw-nummer: <?print_r($app->getInfo('btw'));?><br />
					IBAN: <?print_r($app->getInfo('iban'));?><br />
					Tel.nr.: <?print_r($app->getInfo('phone'));?>
				</p>
				<br>
				<hr>
				<br>
				<strong>
					Er is geld afgeschreven van mijn rekening door <?print_r($app->getInfo('name'));?>?
				</strong>
				<p>
					<?print_r($app->getInfo('name'));?> wordt gebruikt op alle rekeningen van onze dochterbedrijven. Dit zijn: <?print_r($handelsnamen['1'])?>, <?print_r($handelsnamen['2'])?>, <?print_r($handelsnamen['3']);?>, <?print_r($handelsnamen['4']);?>, <?print_r($handelsnamen['5']);?>, <?print_r($handelsnamen['6']);?>.
				</p>
				<br>
				<strong>
					Met wie kan ik contact opnemen?
				</strong>
				<p>
					Een email sturen kan naar <a href="mailto:info@<?print_r(str_replace(' ', '', $app->getInfo('name')));?>.nl"><?print_r($app->getInfo('name'));?></a>.
				</p>
				<br>
				<hr>
				<br>
				<footer>
					<p>
						Deze site is <a href="<?print_r($app->getInfo('git'));?>" target="_blank">open source</a>.
					</p>
					<p>
						Deze site is in beheer van <a href="https://<?print_r(str_replace(' ', '', $handelsnamen['4']));?>.com" target="_blank"><?print_r($handelsnamen['4']);?></a>.
					</p>
				</footer>
			</div>


		<div>
	</body>
</html>

